# Projet Final LINUX

Ce projet est le projet final sur le cours du LINUX. 
Il  a été mené par 3 étudiants : 
* Ivo TALVET *Walid BOUKKA *Elie BENGOU

## Lancement

Pour lancer le projet (La VM) il faut au préable : 

- Configurer le réseau de lancement :  

    1.Aller dans le menu de confuguration 
    ![Configuration](images/img1.png)
    2.Sélecctionner le réseau privé hote
    ![Configuration](images/img2.png)

## Liens utiles

[Les liens utilisés pour le projet](https://gitlab.com/Meithal/tp-linux-vm/-/wikis/home)





