#!/bin/bash

mkdir --parents temp
pushd temp

wget https://github.com/Cockpit-HQ/Cockpit/archive/refs/tags/2.3.8.tar.gz
tar xvzf 2.3.8.tar.gz

sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get install php8.2

pushd Cockpit-2.3.8
#php8.2 -S localhost:8000
./start_app.sh &
