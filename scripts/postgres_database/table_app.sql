create table users
(
 id bigserial not null constraint users_pk primary key,
 name varchar(100) not null,
 password bytea not null,
 created timestamp with time zone not null,
 updated timestamp with time zone not null
);

create unique index users_name_uindex on users (name);

create table articles (

 id bigserial constraint articles_pk primary key,
 slug text not null,
 title text not null,
 content text not null,
 date timestamp with time zone default CURRENT_TIMESTAMP not null,
 active boolean default false not null,
 id_user bigint constraint articles_users_id_fk references users on update cascade,
 created timestamp with time zone default CURRENT_TIMESTAMP not null,
 updated timestamp with time zone default CURRENT_TIMESTAMP not null
);

create unique index articles_slug_uindex on articles(slug);
